package kz.greetgo.sqlmanager.parser2;

import java.net.URL;

import kz.greetgo.sqlmanager.model.Field;
import kz.greetgo.sqlmanager.model.Table;
import kz.greetgo.sqlmanager.parser.StruGenerator;

import org.testng.annotations.Test;

public class StruGeneratorTest {
  @Test
  public void test() throws Exception {
    URL url = getClass().getResource("example.nf3");
    StruGenerator sg = new StruGenerator();
    sg.printPStru = false;
    sg.generate(url);
    
    for (Table t : sg.stru.tables.values()) {
      System.out.println(t.name);
      for (Field field : t.keys) {
        print("*", field);
      }
      for (Field field : t.fields) {
        print(" ", field);
      }
      System.out.println();
    }
    
  }
  
  private void print(String pre, Field field) {
    System.out.println("  " + pre + field.name + ' ' + field.type);
  }
}
