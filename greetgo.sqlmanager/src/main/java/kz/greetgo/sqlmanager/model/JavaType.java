package kz.greetgo.sqlmanager.model;

public interface JavaType {
  String objectType();
  
  String javaType();
}
