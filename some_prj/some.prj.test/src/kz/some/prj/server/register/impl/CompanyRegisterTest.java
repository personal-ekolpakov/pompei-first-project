package kz.some.prj.server.register.impl;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import kz.some.prj.server.entity.Company;
import kz.some.prj.server.register.CompanyRegister;
import kz.some.prj.server.util.Hibernate;
import kz.some.prj.share.model.CompanyFindFilter;
import kz.some.prj.share.model.CompanyRecord;
import kz.some.prj.test.util.AbstractTestWithTransactions;
import kz.some.prj.test.util.beans.TestHelper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.Test;

@ContextConfiguration(locations = { "classpath:context/test.xml" })
public class CompanyRegisterTest extends AbstractTestWithTransactions {
  
  @Autowired
  private Hibernate h;
  
  @Autowired
  private CompanyRegister register;
  
  @Autowired
  private TestHelper th;
  
  @Test
  public void findRecords_byNameFragment() throws Exception {
    
    startWrite();
    
    th.deleteAll(Company.class);
    
    {
      Company c = new Company();
      c.setRegistrationName("ТОО Переход");
      c.setSimpleName("Переход");
      
      h.putNew(c);
    }
    {
      Company c = new Company();
      c.setRegistrationName("fdsfasdf");
      c.setSimpleName("fdsafdsafsd");
      
      h.putNew(c);
    }
    commit();
    
    CompanyFindFilter filter = new CompanyFindFilter();
    filter.setNameFragment("Пере");
    
    List<CompanyRecord> res = register.findRecords(filter, 0, 0);
    
    assertThat(res).isNotNull();
    assertThat(res).hasSize(1);
    
    System.out.println(res);
    
  }
  
  @Test
  public void findRecords_maxCount() throws Exception {
    
    startWrite();
    
    th.deleteAll(Company.class);
    
    for (int i = 0; i < 50; i++) {
      Company c = new Company();
      c.setRegistrationName("dfsfdsf");
      c.setSimpleName("asd");
      h.putNew(c);
    }
    
    commit();
    
    CompanyFindFilter filter = new CompanyFindFilter();
    
    List<CompanyRecord> res = register.findRecords(filter, 0, 5);
    
    assertThat(res).hasSize(5);
  }
  
  @Test
  public void findRecords_offset() throws Exception {
    
    startWrite();
    
    th.deleteAll(Company.class);
    
    for (int i = 0; i < 7; i++) {
      Company c = new Company();
      c.setRegistrationName("dfsfdsf");
      c.setSimpleName("asd" + (i + 1));
      h.putNew(c);
    }
    
    commit();
    
    CompanyFindFilter filter = new CompanyFindFilter();
    
    List<CompanyRecord> res = register.findRecords(filter, 3, 2);
    
    System.out.println(res);
    
    assertThat(res).hasSize(2);
    assertThat(res.get(0).getName()).isEqualTo("asd4");
    assertThat(res.get(1).getName()).isEqualTo("asd5");
  }
}
