package kz.some.prj.test.util.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 * Менеджер транзакций
 * 
 * @author pompei
 */
@Component
public class Transaction {
	@Autowired
	private PlatformTransactionManager txManager;
	
	/**
	 * Начало транзакции для записи. Транзакцию, открытую этим методом, необходимо завершать методом
	 * {@link #commit(TransactionStatus)} или {@link #rollback(TransactionStatus)}
	 * 
	 * @return статус транзакции. Этот объект необходимо передать в метод завершения транзации.
	 */
	public TransactionStatus startWrite() {
		DefaultTransactionDefinition definition =
			new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRED);
		definition.setReadOnly(false);
		return txManager.getTransaction(definition);
	}
	
	/**
	 * Завершает транзакцию записи с фиксацией внесённых изменений.
	 * 
	 * @param status
	 *            объект статуса транзакции (результат метода {@link #startWrite()})
	 */
	public void commit(TransactionStatus status) {
		txManager.commit(status);
	}
	
	/**
	 * Завершает транзакцию записи с отменой внесённых изменений.
	 * 
	 * @param status
	 *            объект статуса транзакции (результат метода {@link #startWrite()})
	 */
	public void rollback(TransactionStatus status) {
		txManager.rollback(status);
	}
	
	/**
	 * Начало транзакции для чтения. Транзакцию, открытую этим методом, необходимо завершать методом
	 * {@link #endRead(TransactionStatus)}
	 * 
	 * @return статус транзакции. Этот объект необходимо передать в метод завершения транзации.
	 */
	public TransactionStatus startRead() {
		DefaultTransactionDefinition definition =
			new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRED);
		definition.setReadOnly(true);
		return txManager.getTransaction(definition);
	}
	
	/**
	 * Завершает транзакцию чтения
	 * 
	 * @param status
	 *            объект статуса транзакции (результат метода {@link #startRead()})
	 */
	public void endRead(TransactionStatus status) {
		rollback(status);
	}
}
