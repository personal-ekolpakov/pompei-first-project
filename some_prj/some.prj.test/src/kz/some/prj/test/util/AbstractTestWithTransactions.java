package kz.some.prj.test.util;

import javax.annotation.Resource;

import kz.some.prj.test.util.beans.Transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.TransactionStatus;
import org.testng.IHookCallBack;
import org.testng.ITestResult;
import org.testng.SkipException;

public abstract class AbstractTestWithTransactions extends AbstractTestNGSpringContextTests {
  
  @Autowired
  private Transaction transaction;
  private TransactionStatus status = null;
  
  @Resource(name = "db.login")
  private String dbLogin;
  
  @Override
  public void run(IHookCallBack callBack, ITestResult testResult) {
    if (dbLogin.endsWith( "real" )) {
      throw new SkipException( "You cannot run tests for user: " + dbLogin );
    }
    super.run( callBack, testResult );
    cleanTransaction();
  }
  
  protected void startWrite() {
    if (status != null) {
      throw new IllegalStateException( "startWrite: Транзакция уже есть: " + status );
    }
    status = transaction.startWrite();
  }
  
  protected void commit() {
    if (status == null) return;
    if (!status.isCompleted()) {
      transaction.commit( status );
    }
    status = null;
  }
  
  protected void startRead() {
    if (status != null) {
      throw new IllegalStateException( "startRead: Транзакция уже есть: " + status );
    }
    status = transaction.startRead();
  }
  
  protected void endRead() {
    if (status == null) return;
    transaction.endRead( status );
    status = null;
  }
  
  private void cleanTransaction() {
    if (status == null) return;
    if (!status.isCompleted()) {
      transaction.rollback( status );
    }
    status = null;
  }
}
