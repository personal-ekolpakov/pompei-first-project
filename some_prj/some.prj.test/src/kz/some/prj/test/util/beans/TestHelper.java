package kz.some.prj.test.util.beans;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import kz.some.prj.server.entity.IdEntity;

import org.hibernate.metadata.ClassMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Component;

@Component
public class TestHelper {
  @Autowired
  private HibernateTemplate ht;
  
  public final Random rnd = new Random();
  
  public String rndStr(int len) {
    StringBuilder sb = new StringBuilder();
    int count = 0;
    while (count < len) {
      {
        sb.append( (char)('A' + rnd.nextInt( 'Z' - 'A' + 1 )) );
        if (++count >= len) break;
      }
      {
        sb.append( (char)('a' + rnd.nextInt( 'z' - 'a' + 1 )) );
        if (++count >= len) break;
      }
      {
        sb.append( (char)('А' + rnd.nextInt( 'Я' - 'А' + 1 )) );
        if (++count >= len) break;
      }
      {
        sb.append( (char)('а' + rnd.nextInt( 'я' - 'а' + 1 )) );
        if (++count >= len) break;
      }
      {
        sb.append( (char)('0' + rnd.nextInt( '9' - '0' + 1 )) );
        if (++count >= len) break;
      }
    }
    return sb.toString();
  }
  
  public String rndIntStr(int len) {
    StringBuilder sb = new StringBuilder();
    int count = 0;
    while (count < len) {
      {
        sb.append( rnd.nextInt( 10 ) );
        if (++count >= len) break;
      }
    }
    return sb.toString();
  }
  
  public <T extends IdEntity> void deleteAll(Class<T> cl) {
    if (cl.getAnnotation( Entity.class ) == null) {
      throw new IllegalArgumentException( "Only entity can be cleaned" );
    }
    
    deleteAll( new HashSet<Class<T>>(), cl );
  }
  
  private <T extends IdEntity> void deleteAll(Set<Class<T>> deletedClasses, Class<T> cl) {
    if (deletedClasses.contains( cl )) return;
    deletedClasses.add( cl );
    
    for (Class<T> subCl : getDependClasses( cl )) {
      deleteAll( deletedClasses, subCl );
    }
    
    ht.deleteAll( ht.find( "from " + cl.getName() ) );
  }
  
  private <T extends IdEntity> Set<Class<T>> getDependClasses(Class<T> cl) {
    Set<Class<T>> ret = new HashSet<Class<T>>();
    
    for (Class<T> he : this.<T> getAllEntityClasses()) {
      if (he.equals( cl )) continue;
      if (getManyToOneClasses( he ).contains( cl )) {
        ret.add( he );
      }
    }
    
    return ret;
  }
  
  @SuppressWarnings("unchecked")
  public <T extends IdEntity> Set<Class<T>> getAllEntityClasses() {
    Map<String, ClassMetadata> all = ht.getSessionFactory().getAllClassMetadata();
    
    Set<Class<T>> ret = new HashSet<Class<T>>();
    
    for (Entry<String, ClassMetadata> e : all.entrySet()) {
      try {
        ret.add( (Class<T>)Class.forName( e.getValue().getEntityName() ) );
      } catch (ClassNotFoundException e1) {
        throw new RuntimeException( e1 );
      }
    }
    
    return ret;
  }
  
  @SuppressWarnings("unchecked")
  private <T extends IdEntity> Set<Class<T>> getManyToOneClasses(Class<T> cl) {
    Set<Class<T>> ret = new HashSet<Class<T>>();
    
    for (Method method : cl.getMethods()) {
      if (method.getAnnotation( ManyToOne.class ) == null) continue;
      if (!method.getName().startsWith( "get" )) continue;
      ret.add( (Class<T>)method.getReturnType() );
    }
    
    return ret;
  }
  
  public Date rndDateTime() {
    int year = 1990 + rnd.nextInt( 2017 - 1990 + 1 );
    int month = 1 + rnd.nextInt( 12 );
    int day = 1 + rnd.nextInt( 28 );
    int hour = rnd.nextInt( 24 );
    int minute = rnd.nextInt( 60 );
    int second = rnd.nextInt( 60 );
    
    DateFormat fmt = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
    
    try {
      return fmt.parse( year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second );
    } catch (ParseException e) {
      throw new RuntimeException( e );
    }
  }
  
  public <T> T rndElem(T[] values) {
    return values[rnd.nextInt( values.length )];
  }
  
}
