package kz.some.prj.share.model;

import kz.some.prj.share.util.HasDisplayStr;

public enum PhoneType implements HasDisplayStr {
  HOME("Домашний"), MOBILE("Мобильный"), WORK("Рабочий");
  
  private final String displayStr;
  
  private PhoneType(String displayStr) {
    this.displayStr = displayStr;
  }
  
  @Override
  public String getDisplayStr() {
    return displayStr;
  }
}
