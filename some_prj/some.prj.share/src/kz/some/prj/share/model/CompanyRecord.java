package kz.some.prj.share.model;

public class CompanyRecord {
  private String id;
  private String displayStr;
  private String tooltipStr;
  private String phones;
  private int empoyeeCount;
  
  @Override
  public String toString() {
    return "CompanyRecord [id=" + id + ", displayStr=" + displayStr + ", tooltipStr=" + tooltipStr
        + ", phones=" + phones + ", empoyeeCount=" + empoyeeCount + "]";
  }
  
  public String getId() {
    return id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public String getName() {
    return displayStr;
  }
  
  public void setDisplayStr(String name) {
    this.displayStr = name;
  }
  
  public String getPhones() {
    return phones;
  }
  
  public void setPhones(String phones) {
    this.phones = phones;
  }
  
  public String getTooltipStr() {
    return tooltipStr;
  }
  
  public void setTooltipStr(String tooltipStr) {
    this.tooltipStr = tooltipStr;
  }
  
  public int getEmpoyeeCount() {
    return empoyeeCount;
  }
  
  public void setEmpoyeeCount(int empoyeeCount) {
    this.empoyeeCount = empoyeeCount;
  }
}
