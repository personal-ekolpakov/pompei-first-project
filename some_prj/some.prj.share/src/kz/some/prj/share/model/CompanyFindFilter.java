package kz.some.prj.share.model;

public class CompanyFindFilter {
  private String nameFragment;
  private Integer employeeCountFrom, employeeCountTo;
  
  private CompanyListOrd order;
  private boolean ascending;
  
  public String getNameFragment() {
    return nameFragment;
  }
  
  public void setNameFragment(String nameFragment) {
    this.nameFragment = nameFragment;
  }
  
  public Integer getEmployeeCountFrom() {
    return employeeCountFrom;
  }
  
  public void setEmployeeCountFrom(Integer employeeCountFrom) {
    this.employeeCountFrom = employeeCountFrom;
  }
  
  public Integer getEmployeeCountTo() {
    return employeeCountTo;
  }
  
  public void setEmployeeCountTo(Integer employeeCountTo) {
    this.employeeCountTo = employeeCountTo;
  }
  
  public CompanyListOrd getOrder() {
    return order;
  }
  
  public void setOrder(CompanyListOrd order) {
    this.order = order;
  }
  
  public boolean isAscending() {
    return ascending;
  }
  
  public void setAscending(boolean ascending) {
    this.ascending = ascending;
  }
}
