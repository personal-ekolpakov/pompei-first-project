package kz.some.prj.share.util;

public interface HasDisplayStr {
  String getDisplayStr();
}
