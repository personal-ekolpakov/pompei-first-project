package kz.some.prj.server.register.impl;

import java.util.ArrayList;
import java.util.List;

import kz.some.prj.server.entity.Company;
import kz.some.prj.server.register.CompanyRegister;
import kz.some.prj.server.util.Hibernate;
import kz.some.prj.share.model.CompanyFindFilter;
import kz.some.prj.share.model.CompanyRecord;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class CompanyRegisterImpl implements CompanyRegister {
  
  @Autowired
  private Hibernate h;
  
  @Override
  public List<CompanyRecord> findRecords(CompanyFindFilter filter, int offset, int maxResultSize) {
    List<CompanyRecord> ret = new ArrayList<CompanyRecord>();
    
    DetachedCriteria cri = DetachedCriteria.forClass(Company.class);
    
    if (filter.getNameFragment() != null) {
      cri.add(Restrictions.like("simpleName", filter.getNameFragment(), MatchMode.START));
    }
    
    if (maxResultSize == 0) {
      for (Company c : h.cri(Company.class, cri)) {
        ret.add(c.toRecord());
      }
    } else {
      for (Company c : h.cri(Company.class, cri, offset, maxResultSize)) {
        ret.add(c.toRecord());
      }
    }
    
    return ret;
  }
  
}
