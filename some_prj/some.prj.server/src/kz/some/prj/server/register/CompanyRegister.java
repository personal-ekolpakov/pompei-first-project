package kz.some.prj.server.register;

import java.util.List;

import kz.some.prj.share.model.CompanyFindFilter;
import kz.some.prj.share.model.CompanyRecord;

public interface CompanyRegister {
  List<CompanyRecord> findRecords(CompanyFindFilter filter, int offset, int maxResultSize);
}
