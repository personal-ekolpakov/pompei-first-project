package kz.some.prj.server.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@SuppressWarnings("serial")
public class WorkplacePhone extends IdEntity {
  private Workplace owner;
  private Phone phone;
  
  @ManyToOne(optional = false)
  public Workplace getOwner() {
    return owner;
  }
  
  public void setOwner(Workplace owner) {
    this.owner = owner;
  }
  
  @ManyToOne(optional = false)
  public Phone getPhone() {
    return phone;
  }
  
  public void setPhone(Phone phone) {
    this.phone = phone;
  }
}
