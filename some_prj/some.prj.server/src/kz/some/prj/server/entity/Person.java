package kz.some.prj.server.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
@SuppressWarnings("serial")
public class Person extends IdEntity {
  private String family, name, paronymic;
  private Date bithdate;
  
  private Set<PersonPhone> phones = new HashSet<PersonPhone>();
  
  public String getFamily() {
    return family;
  }
  
  public void setFamily(String family) {
    this.family = family;
  }
  
  @Column(nullable = false)
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getParonymic() {
    return paronymic;
  }
  
  public void setParonymic(String paronymic) {
    this.paronymic = paronymic;
  }
  
  public Date getBithdate() {
    return bithdate;
  }
  
  public void setBithdate(Date bithdate) {
    this.bithdate = bithdate;
  }
  
  @OneToMany(mappedBy = "owner")
  public Set<PersonPhone> getPhones() {
    return phones;
  }
  
  public void setPhones(Set<PersonPhone> phones) {
    this.phones = phones;
  }
}
