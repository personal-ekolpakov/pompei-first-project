package kz.some.prj.server.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Index;

@Entity
@SuppressWarnings("serial")
public class Account extends IdEntity {
  private String login;
  private String passwordMd5sum;
  private Person owner;
  
  @Index(name = "Account_login")
  @Column(nullable = false, unique = true, length = 100)
  public String getLogin() {
    return login;
  }
  
  public void setLogin(String login) {
    this.login = login;
  }
  
  @ManyToOne(optional = false)
  public Person getOwner() {
    return owner;
  }
  
  public void setOwner(Person owner) {
    this.owner = owner;
  }
  
  public String getPasswordMd5sum() {
    return passwordMd5sum;
  }
  
  public void setPasswordMd5sum(String passwordMd5sum) {
    this.passwordMd5sum = passwordMd5sum;
  }
}
