package kz.some.prj.server.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import kz.some.prj.server.util.RndStr;

@MappedSuperclass
@SuppressWarnings("serial")
public class IdEntity implements Serializable {
  private String id;
  
  //(10 + 26 + 26)^16 > 10^28 - вполне большое число, чтобы дважды не повторился один и тотже ИД
  private static final RndStr rndStr = new RndStr( 16 );
  
  public void generateId() {
    if (id != null) throw new IllegalStateException( "Id already generated" );
    setId( rndStr.nextString() );
  }
  
  @Id
  @Column(length = 30)
  public String getId() {
    return id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 :id.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (!(obj instanceof IdEntity)) return false;
    
    IdEntity other = (IdEntity)obj;
    
    if (getId() == null) return other.getId() == null;
    
    return getId().equals( other.getId() );
  }
}
