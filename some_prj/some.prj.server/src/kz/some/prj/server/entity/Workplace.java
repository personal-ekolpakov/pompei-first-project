package kz.some.prj.server.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@SuppressWarnings("serial")
public class Workplace extends IdEntity {
  private Company owner;
  private Post post;
  private Person heldBy;
  
  private Set<WorkplacePhone> phones = new HashSet<WorkplacePhone>();
  
  @ManyToOne(optional = false)
  public Post getPost() {
    return post;
  }
  
  public void setPost(Post post) {
    this.post = post;
  }
  
  @ManyToOne(optional = false)
  public Company getOwner() {
    return owner;
  }
  
  public void setOwner(Company owner) {
    this.owner = owner;
  }
  
  @OneToMany(mappedBy = "owner")
  public Set<WorkplacePhone> getPhones() {
    return phones;
  }
  
  public void setPhones(Set<WorkplacePhone> phones) {
    this.phones = phones;
  }
  
  @ManyToOne
  public Person getHeldBy() {
    return heldBy;
  }
  
  public void setHeldBy(Person heldBy) {
    this.heldBy = heldBy;
  }
}
