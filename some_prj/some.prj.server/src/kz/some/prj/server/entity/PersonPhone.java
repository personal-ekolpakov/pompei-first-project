package kz.some.prj.server.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@SuppressWarnings("serial")
public class PersonPhone extends IdEntity {
  private Person owner;
  private Phone phone;
  
  @ManyToOne(optional = false)
  public Person getOwner() {
    return owner;
  }
  
  public void setOwner(Person owner) {
    this.owner = owner;
  }
  
  @ManyToOne(optional = false)
  public Phone getPhone() {
    return phone;
  }
  
  public void setPhone(Phone phone) {
    this.phone = phone;
  }
}
