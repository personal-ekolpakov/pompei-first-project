package kz.some.prj.server.entity;

import javax.persistence.Entity;

@Entity
@SuppressWarnings("serial")
public class Post extends IdEntity {
  private String name;
  private String code;
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getCode() {
    return code;
  }
  
  public void setCode(String code) {
    this.code = code;
  }
}
