package kz.some.prj.server.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import kz.some.prj.share.model.CompanyRecord;

import org.hibernate.annotations.Index;

@Entity
@SuppressWarnings("serial")
public class Company extends IdEntity {
  private String simpleName;//Microsoft
  private String shortName;//MS
  private String registrationName;//АО "Корпорация MicroSoft"
  
  private Set<Workplace> workplaces = new HashSet<Workplace>();
  
  @Column(nullable = false)
  @Index(name = "Company_employee_count")
  public Integer getCalcEmployeeCount() {
    int ret = 0;
    for (Workplace x : getWorkplaces()) {
      if (x.getHeldBy() != null) ret++;
    }
    return ret;
  }
  
  public void setCalcEmployeeCount(Integer value) {}
  
  @Column(nullable = false)
  @Index(name = "Company_simpleName")
  public String getSimpleName() {
    return simpleName;
  }
  
  public void setSimpleName(String simpleName) {
    this.simpleName = simpleName;
  }
  
  public String getShortName() {
    return shortName;
  }
  
  public void setShortName(String shortName) {
    this.shortName = shortName;
  }
  
  @Column(nullable = false)
  @Index(name = "Company_registrationName")
  public String getRegistrationName() {
    return registrationName;
  }
  
  public void setRegistrationName(String fullName) {
    this.registrationName = fullName;
  }
  
  @OneToMany(mappedBy = "owner")
  public Set<Workplace> getWorkplaces() {
    return workplaces;
  }
  
  public void setWorkplaces(Set<Workplace> workplaces) {
    this.workplaces = workplaces;
  }
  
  public CompanyRecord toRecord() {
    CompanyRecord ret = new CompanyRecord();
    ret.setId(getId());
    ret.setDisplayStr(getSimpleName());
    ret.setEmpoyeeCount(getCalcEmployeeCount());
    ret.setPhones("-");
    ret.setTooltipStr(getRegistrationName());
    return ret;
  }
}
