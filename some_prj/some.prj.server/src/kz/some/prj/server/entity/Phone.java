package kz.some.prj.server.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import kz.some.prj.share.model.PhoneType;

@Entity
@SuppressWarnings("serial")
public class Phone extends IdEntity {
  private PhoneType type;
  private String value;
  
  @Enumerated(EnumType.STRING)
  @Column(length = 50, nullable = false)
  public PhoneType getType() {
    return type;
  }
  
  public void setType(PhoneType type) {
    this.type = type;
  }
  
  @Column(length = 20, nullable = false)
  public String getValue() {
    return value;
  }
  
  public void setValue(String value) {
    this.value = value;
  }
}
