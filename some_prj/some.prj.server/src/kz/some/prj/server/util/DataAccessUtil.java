package kz.some.prj.server.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.NoResultException;

import kz.some.prj.server.entity.IdEntity;

import org.springframework.dao.support.DataAccessUtils;

public class DataAccessUtil {
  
  /**
   * Возвращает один элемент из коллекции или null, если коллекция пустая.
   * Генерирует ошибку, если в коллекции несколько элементов. Генерирует ошибку,
   * если тип элемента не соответствует запрашиваемому
   * 
   * @param <T>
   *          тип возвращаемого значения
   * @param requiredType
   *          тип возвращаемого значения
   * @param results
   *          исходная коллекция
   * @return один элемент или null
   */
  @SuppressWarnings("unchecked")
  public static <T> T oneOrNull(Class<T> requiredType,
      @SuppressWarnings("rawtypes") Collection results) {
    if (results == null) return null;
    if (results.size() > 1) {
      throw new TooManyResults( 1, results.size() );
    }
    if (results.size() == 0) return null;
    Object retObject = results.iterator().next();
    if (retObject == null) return null;
    if (requiredType != null && !requiredType.isInstance( retObject )) {
      throw new WrongeTypeException( requiredType, retObject.getClass() );
    }
    return (T)retObject;
  }
  
  /**
   * Возвращает один элемент из коллекции или генерирует ошибку, если коллекция
   * пустая. Генерирует ошибку, если в коллекции несколько элементов. Генерирует
   * ошибку, если тип элемента не соответствует запрашиваемому
   * 
   * @param <T>
   *          тип возвращаемого значения
   * @param requiredType
   *          тип возвращаемого значения
   * @param results
   *          исходная коллекция
   * @return элемент из коллекции
   */
  @SuppressWarnings("unchecked")
  public static <T> T oneOrFail(Class<T> requiredType,
      @SuppressWarnings("rawtypes") Collection results) {
    if (results == null) {
      throw new NullPointerException( "results must be defined" );
    }
    if (results.size() > 1) {
      throw new TooManyResults( 1, results.size() );
    }
    if (results.size() == 0) throw new NoResultException();
    Object retObject = results.iterator().next();
    if (retObject == null) return null;
    if (requiredType != null && !requiredType.isInstance( retObject )) {
      throw new WrongeTypeException( requiredType, retObject.getClass() );
    }
    return (T)retObject;
  }
  
  /**
   * Преобразует список результатов в мапу, ключами которой являются ИД объектов
   * 
   * @param <T>
   *          тип элемента списка
   * @param results
   *          список
   * @return мапа
   */
  @SuppressWarnings("unchecked")
  public static <T extends IdEntity> Map<String, T> toMap(
      @SuppressWarnings("rawtypes") Collection results) {
    Map<String, T> ret = new HashMap<String, T>();
    for (Object object : results) {
      T t = (T)object;
      ret.put( t.getId(), t );
    }
    return ret;
  }
  
  /**
   * Производит типизацию коллекции
   * 
   * @param <T>
   *          тип элементов коллекции
   * @param classs
   *          тип элементов коллекции
   * @param results
   *          нетипизированная коллекция
   * @return типизированная коллекция
   */
  @SuppressWarnings("unchecked")
  public static <T> Collection<T> typed(Class<T> classs,
      @SuppressWarnings("rawtypes") Collection results) {
    return results;
  }
  
  /**
   * Производит типизацию коллекции в Long
   * 
   * @param results
   *          нетипизированная коллекция
   * @return типизированная коллекция
   */
  @SuppressWarnings("unchecked")
  public static Collection<Long> longs(@SuppressWarnings("rawtypes") Collection results) {
    return results;
  }
  
  /**
   * Производит типизацию коллекции в String
   * 
   * @param results
   *          нетипизированная коллекция
   * @return типизированная коллекция
   */
  @SuppressWarnings("unchecked")
  public static Collection<String> strs(@SuppressWarnings("rawtypes") Collection results) {
    return results;
  }
  
  /**
   * Возвращает первый элемент коллекции или null, если в коллекции нет
   * элементов. Генерирует ошибку, если тип элемента не соответствует
   * запрашиваемому
   * 
   * @param <T>
   *          тип элементов коллекции
   * @param requiredType
   *          тип элементов коллекции
   * @param results
   *          исходная коллекция
   * @return первый элемент из коллекции
   */
  @SuppressWarnings("unchecked")
  public static <T> T firstOrNull(Class<T> requiredType,
      @SuppressWarnings("rawtypes") Collection results) {
    if (results == null) return null;
    if (results.size() == 0) return null;
    Object retObject = results.iterator().next();
    if (requiredType != null && !requiredType.isInstance( retObject )) {
      throw new WrongeTypeException( requiredType, retObject.getClass() );
    }
    return (T)retObject;
  }
  
  /**
   * Возвращает первый элемент коллекции или генерирует ошибку, если в коллекции
   * нет элементов. Генерирует ошибку, если тип элемента не соответствует
   * запрашиваемому
   * 
   * @param <T>
   *          тип элементов коллекции
   * @param requiredType
   *          тип элементов коллекции
   * @param results
   *          исходная коллекция
   * @return первый элемент из коллекции
   */
  public static <T> T firstOrFail(Class<T> requiredType,
      @SuppressWarnings("rawtypes") Collection results) {
    T ret = firstOrNull( requiredType, results );
    if (ret == null) throw new NoResultException();
    return ret;
  }
  
  /**
   * Возвращает первый эелемент коллекции как число или ноль
   * 
   * @param results
   *          исходная коллекция
   * @return первый элемент из коллекции
   */
  public static int intOrZero(@SuppressWarnings("rawtypes") Collection results) {
    try {
      return DataAccessUtils.intResult( results );
    } catch (RuntimeException e) {
      return 0;
    }
  }
  
  /**
   * Получает первый объект колекции с элементами типа Long
   * 
   * @param results
   *          исходная коллекция
   * @return первый элемент из коллекции или null
   */
  public static Long longOrNull(@SuppressWarnings("rawtypes") Collection results) {
    return oneOrNull( Long.class, results );
  }
}
