package kz.some.prj.server.util;

import static kz.some.prj.server.util.DataAccessUtil.typed;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import kz.some.prj.server.entity.IdEntity;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;

public class Hibernate {
  @Autowired
  private HibernateTemplate source;
  
  public <T> T load(Class<T> entityClass, Serializable id) throws DataAccessException {
    return source.load( entityClass, id );
  }
  
  public void delete(Object entity) throws DataAccessException {
    source.delete( entity );
  }
  
  public void deleteAll(@SuppressWarnings("rawtypes") final Collection entities)
      throws DataAccessException {
    source.deleteAll( entities );
  }
  
  @SuppressWarnings("rawtypes")
  public List find(String queryString) throws DataAccessException {
    return find( queryString, (Object[])null );
  }
  
  @SuppressWarnings("rawtypes")
  public List find(String queryString, Object value) throws DataAccessException {
    return find( queryString, new Object[] { value } );
  }
  
  @SuppressWarnings("rawtypes")
  public List find(final String queryString, final Object... values) throws DataAccessException {
    return source.find( queryString, values );
  }
  
  public <T> Collection<T> from(Class<T> classs, String whereString, final Object... values)
      throws DataAccessException {
    whereString = whereString == null ? "" :" where " + whereString;
    return typed( classs, source.find( "from " + classs.getName() + whereString, values ) );
  }
  
  public <T> Collection<T> list(Class<T> classs, String query, final Object... values)
      throws DataAccessException {
    return typed( classs, source.find( query, values ) );
  }
  
  public <T> Collection<T> from(Class<T> classs, String whereString, final Object value)
      throws DataAccessException {
    whereString = whereString == null ? "" :" where " + whereString;
    return typed( classs, source.find( "from " + classs.getName() + whereString, value ) );
  }
  
  public <T> Collection<T> list(Class<T> classs, String query, final Object value)
      throws DataAccessException {
    return typed( classs, source.find( query, value ) );
  }
  
  public <T> Collection<T> from(Class<T> classs, String whereString) throws DataAccessException {
    whereString = whereString == null ? "" :" where " + whereString;
    return typed( classs, source.find( "from " + classs.getName() + whereString ) );
  }
  
  public <T> Collection<T> list(Class<T> classs, String query) throws DataAccessException {
    return typed( classs, source.find( query ) );
  }
  
  public Collection<Long> longs(String queryString) {
    return DataAccessUtil.longs( source.find( queryString ) );
  }
  
  public Collection<Long> longs(String queryString, Object... values) {
    return DataAccessUtil.longs( source.find( queryString, values ) );
  }
  
  public Collection<Long> longs(String queryString, Object value) {
    return DataAccessUtil.longs( source.find( queryString, value ) );
  }
  
  public Collection<String> strs(String queryString) {
    return DataAccessUtil.strs( source.find( queryString ) );
  }
  
  public Collection<String> strs(String queryString, Object... values) {
    return DataAccessUtil.strs( source.find( queryString, values ) );
  }
  
  public Collection<String> strs(String queryString, Object value) {
    return DataAccessUtil.strs( source.find( queryString, value ) );
  }
  
  public <T> T firstOrNullFrom(Class<T> classs, String whereString) throws DataAccessException {
    return DataAccessUtil.firstOrNull( classs, from( classs, whereString ) );
  }
  
  public <T> T firstOrNullFrom(Class<T> classs, String whereString, Object value)
      throws DataAccessException {
    return DataAccessUtil.firstOrNull( classs, from( classs, whereString, value ) );
  }
  
  public <T> T firstOrNullFrom(Class<T> classs, String whereString, Object... values)
      throws DataAccessException {
    return DataAccessUtil.firstOrNull( classs, from( classs, whereString, values ) );
  }
  
  public <T> T firstOrFailFrom(Class<T> classs, String whereString) throws DataAccessException {
    return DataAccessUtil.firstOrFail( classs, from( classs, whereString ) );
  }
  
  public <T> T firstOrFailFrom(Class<T> classs, String whereString, Object... values)
      throws DataAccessException {
    return DataAccessUtil.firstOrFail( classs, from( classs, whereString, values ) );
  }
  
  public <T> T firstOrFailFrom(Class<T> classs, String whereString, Object value)
      throws DataAccessException {
    return DataAccessUtil.firstOrFail( classs, from( classs, whereString, value ) );
  }
  
  public <T> T oneOrNullFrom(Class<T> classs, String whereString) throws DataAccessException {
    return DataAccessUtil.oneOrNull( classs, from( classs, whereString ) );
  }
  
  public <T> T oneOrNullFrom(Class<T> classs, String whereString, Object value)
      throws DataAccessException {
    return DataAccessUtil.oneOrNull( classs, from( classs, whereString, value ) );
  }
  
  public <T> T oneOrNullFrom(Class<T> classs, String whereString, Object... values)
      throws DataAccessException {
    return DataAccessUtil.oneOrNull( classs, from( classs, whereString, values ) );
  }
  
  public <T> T oneOrFailFrom(Class<T> classs, String whereString) throws DataAccessException {
    return DataAccessUtil.oneOrFail( classs, from( classs, whereString ) );
  }
  
  public <T> T oneOrFailFrom(Class<T> classs, String whereString, Object... values)
      throws DataAccessException {
    return DataAccessUtil.oneOrFail( classs, from( classs, whereString, values ) );
  }
  
  public <T> T oneOrFailFrom(Class<T> classs, String whereString, Object value)
      throws DataAccessException {
    return DataAccessUtil.oneOrFail( classs, from( classs, whereString, value ) );
  }
  
  @SuppressWarnings("rawtypes")
  public List findByCriteria(DetachedCriteria criteria, int firstResult, int maxResults) {
    return source.findByCriteria( criteria, firstResult, maxResults );
  }
  
  @SuppressWarnings("rawtypes")
  public List findByCriteria(DetachedCriteria criteria) throws DataAccessException {
    return source.findByCriteria( criteria );
  }
  
  public <T> Collection<T> cri(Class<T> classs, DetachedCriteria cri) {
    return typed( classs, source.findByCriteria( cri ) );
  }
  
  public <T> Collection<T> cri(Class<T> classs, DetachedCriteria cri, int firstResult,
      int maxResults) {
    return typed( classs, source.findByCriteria( cri, firstResult, maxResults ) );
  }
  
  public int intOrZero(DetachedCriteria cri) {
    return DataAccessUtil.intOrZero( source.findByCriteria( cri ) );
  }
  
  public <T> T put(T entity) throws DataAccessException {
    source.persist( entity );
    return entity;
  }
  
  public <T extends IdEntity> T putNew(T entity) throws DataAccessException {
    entity.generateId();
    return put( entity );
  }
  
  public <T> T firstOrNull(Class<T> classs, DetachedCriteria cri) {
    return DataAccessUtil.firstOrNull( classs, source.findByCriteria( cri ) );
  }
  
  public <T> T firstOrFail(Class<T> classs, DetachedCriteria cri) {
    return DataAccessUtil.firstOrFail( classs, source.findByCriteria( cri ) );
  }
  
  public <T> T oneOrNull(Class<T> classs, DetachedCriteria cri) {
    return DataAccessUtil.oneOrNull( classs, source.findByCriteria( cri ) );
  }
  
  public <T> T oneOrFail(Class<T> classs, DetachedCriteria cri) {
    return DataAccessUtil.oneOrFail( classs, source.findByCriteria( cri ) );
  }
  
  public int intOrZero(String queryString, Object... values) {
    return DataAccessUtil.intOrZero( source.find( queryString, values ) );
  }
  
  public Collection<Long> longs(DetachedCriteria cri) {
    return DataAccessUtil.longs( source.findByCriteria( cri ) );
  }
}
