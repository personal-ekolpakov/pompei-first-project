package kz.some.prj.server.util;

import org.hibernate.HibernateException;
import org.hibernate.event.EventSource;
import org.hibernate.event.def.DefaultFlushEventListener;

/**
 * Patches Hibernate to prevent this issue
 * http://opensource.atlassian.com/projects/hibernate/browse/HHH-2763
 * 
 * TO DO: Remove this patch when HHH-2763 is resolved
 * 
 * @author Graeme Rocher
 * @since 1.2
 */
public class PatchedDefaultFlushEventListener extends DefaultFlushEventListener {
  private static final long serialVersionUID = 1729318538471679460L;
  
  @Override
  protected void performExecutions(EventSource session) throws HibernateException {
    session.getPersistenceContext().setFlushing( true );
    try {
      session.getJDBCContext().getConnectionManager().flushBeginning();
      // we need to lock the collection caches before
      // executing entity inserts/updates in order to
      // account for bidi associations
      session.getActionQueue().prepareActions();
      session.getActionQueue().executeActions();
    } catch (HibernateException he) {
      //LOG.error("Could not synchronize database state with session", he);
      throw he;
    } finally {
      session.getPersistenceContext().setFlushing( false );
      session.getJDBCContext().getConnectionManager().flushEnding();
    }
  }
}
