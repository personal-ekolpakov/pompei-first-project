package kz.some.prj.server.util;

public class WrongeTypeException extends RuntimeException {
	private static final long serialVersionUID = 5296647323371598173L;

	public WrongeTypeException(Class<?> expectedType, Class<?> actualType) {
		super("Wrone type: expected type: " + expectedType + ", actual type: "
			+ actualType);
	}
	
}
