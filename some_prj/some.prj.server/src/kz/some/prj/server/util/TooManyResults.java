package kz.some.prj.server.util;

public class TooManyResults extends RuntimeException {
	private static final long serialVersionUID = -7849940374995725218L;
	
	public TooManyResults(int expectedSize, int actualSize) {
		super("Too many results: expected size: " + expectedSize + ", actual size: " + actualSize);
	}
	
}
