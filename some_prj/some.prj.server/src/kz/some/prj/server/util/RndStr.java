package kz.some.prj.server.util;

import java.security.SecureRandom;

public class RndStr {
  private static final char[] symbols = new char[10 + 26 + 26];
  static {
    int index = 0;
    for (int i = 0; i < 10; ++i) {
      symbols[index++] = (char)('0' + i);
    }
    for (int i = 0; i < 26; ++i) {
      symbols[index++] = (char)('a' + i);
    }
    for (int i = 0; i < 26; ++i) {
      symbols[index++] = (char)('A' + i);
    }
  }
  
  private static volatile SecureRandom numberGenerator = null;
  private final char[] buf;
  
  public RndStr(int length) {
    if (length < 1) throw new IllegalArgumentException( "length < 1: " + length );
    buf = new char[length];
  }
  
  public String nextString() {
    SecureRandom ng = numberGenerator;
    if (ng == null) {
      numberGenerator = ng = new SecureRandom();
    }
    
    for (int idx = 0; idx < buf.length; ++idx) {
      buf[idx] = symbols[ng.nextInt( symbols.length )];
    }
    
    return new String( buf );
  }
  
}
