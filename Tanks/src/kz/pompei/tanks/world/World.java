package kz.pompei.tanks.world;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

public class World {
  
  @SuppressWarnings("unused")
  private long startedAt;
  
  private List<Figure> walls = new ArrayList<>();
  
  public World() {
    startedAt = System.currentTimeMillis();
  }
  
  public void paint(Graphics2D g, int width, int height) {
    IntTransformer transformer = new IntTransformer(width, height);
    for (Figure figure : walls) {
      figure.paint(g, transformer);
    }
  }
  
  public void timeGo(long currentTimeMillis) {
    // TODO implement
    
  }
  
  public void init() {
    // TODO implement
    
  }
  
}
