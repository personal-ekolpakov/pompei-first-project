package kz.pompei.tanks.world;

import java.awt.Graphics2D;

public abstract class Figure {
  
  protected final Geom geom = new Geom();
  
  public abstract void paint(Graphics2D g, IntTransformer transformer);
}
