package kz.pompei.tanks;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import kz.pompei.tanks.world.World;

@SuppressWarnings("serial")
public class MainForm extends JFrame {
  public static void main(String[] args) {
    new MainForm().setVisible(true);
  }
  
  private boolean repainting = true;
  
  public MainForm() {
    setTitle("Title");
    setSize(800, 600);
    setLocation(100, 100);
    
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        repainting = false;
        dispose();
      }
    });
    
    final World world = new World();
    
    world.init();
    
    setContentPane(new MainPanel(world));
    
    new Thread(new Runnable() {
      @Override
      public void run() {
        while (repainting) {
          try {
            Thread.sleep(30);
          } catch (InterruptedException e) {}
          repaint();
        }
      }
    }).start();
    
    new Thread(new Runnable() {
      @Override
      public void run() {
        while (repainting) {
          try {
            Thread.sleep(10);
          } catch (InterruptedException e) {}
          world.timeGo(System.currentTimeMillis());
        }
      }
    }).start();
  }
}
