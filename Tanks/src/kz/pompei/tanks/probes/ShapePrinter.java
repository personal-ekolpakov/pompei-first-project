package kz.pompei.tanks.probes;

import java.awt.Shape;
import java.awt.geom.PathIterator;
import java.io.PrintStream;

public class ShapePrinter {
  public static void print(PrintStream out, Shape shape) {
    double coords[] = new double[6];
    for (PathIterator iter = shape.getPathIterator(null); !iter.isDone(); iter.next()) {
      int currentSegment = iter.currentSegment(coords);
      switch (currentSegment) {
      case PathIterator.SEG_MOVETO:
        out.println("MOVETO " + "(" + coords[0] + ", " + coords[1] + ")");
        break;
      
      case PathIterator.SEG_LINETO:
        out.println("LINETO " + "(" + coords[0] + ", " + coords[1] + ")");
        break;
      
      case PathIterator.SEG_QUADTO:
        out.println("QUADTO " + "(" + coords[0] + ", " + coords[1] + ") - (" + coords[2] + ", "
            + coords[3] + ")");
        break;
      
      case PathIterator.SEG_CUBICTO:
        out.println("CUBICTO " + "(" + coords[0] + ", " + coords[1] + ") - (" + coords[2] + ", "
            + coords[3] + ") - (" + coords[4] + ", " + coords[5] + ")");
        break;
      
      case PathIterator.SEG_CLOSE:
        out.println("CLOSE");
        break;
      
      default:
        out.println("Unknown " + currentSegment);
        break;
      }
    }
  }
}
