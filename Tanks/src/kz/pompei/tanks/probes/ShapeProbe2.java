package kz.pompei.tanks.probes;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

public class ShapeProbe2 {
  public static void main(String[] args) throws Exception {
    new ShapeProbe2().run();
    System.out.println("OK");
  }
  
  private void run() throws Exception {
    BufferedImage im = new BufferedImage(800, 600, BufferedImage.TYPE_INT_ARGB);
    
    Graphics2D g = im.createGraphics();
    
    Font font = new Font("Norasi", Font.PLAIN, 18);
    g.setFont(font);
    
    FontMetrics fm = g.getFontMetrics();
    
    GlyphVector gv = g.getFont().createGlyphVector(fm.getFontRenderContext(), "Hello world!!!");
    
    Shape outline = gv.getOutline();
    ShapePrinter.print(System.out, outline);
    
    Area text = new Area();
    
    for (int i = 0, C = gv.getNumGlyphs(); i < C; i++) {
      Shape shape = gv.getGlyphOutline(i);
      System.out.println("------------------------> GLIPH " + i);
      ShapePrinter.print(System.out, shape);
      
      text.add(new Area(shape));
    }
    
    {
      Graphics2D g2 = (Graphics2D)g.create();
      
      AffineTransform t = AffineTransform.getScaleInstance(8, 8);
      t.concatenate(AffineTransform.getRotateInstance(10 * Math.PI / 180));
      t.concatenate(AffineTransform.getTranslateInstance(1, 20));
      text.transform(t);
      
      g2.setColor(Color.white);
      g2.fillRect(0, 0, im.getWidth(), im.getHeight());
      g2.clip(text);
      g2.setColor(Color.black);
      g2.fillRect(0, 0, im.getWidth(), im.getHeight());
      
      g2.dispose();
    }
    
    g.dispose();
    
    ImageIO.write(im, "png", new File("/home/pompei/tmp/asd.png"));
  }
}
