package kz.pompei.tanks.probes;

import java.awt.Rectangle;
import java.awt.geom.Area;
import java.awt.geom.PathIterator;

public class ShapeProbe {
  public static void main(String[] args) {
    Rectangle r1 = new Rectangle(5, 5, 20, 10);
    Rectangle r2 = new Rectangle(15, 20, 20, 10);
    
    Area res = new Area(r1);
    //res.intersect(new Area(r2));
    res.add(new Area(r2));
    
    System.out.println("isEmpty = " + res.isEmpty());
    
    PathIterator iter = res.getPathIterator(null);
    for (; !iter.isDone(); iter.next()) {
      double coords[] = new double[6];
      int currentSegment = iter.currentSegment(coords);
      switch (currentSegment) {
      case PathIterator.SEG_CLOSE:
        System.out.println("CLOSE");
        break;
      
      case PathIterator.SEG_MOVETO:
        System.out.println("MOVETO (" + coords[0] + ", " + coords[1] + ")");
        break;
      
      case PathIterator.SEG_LINETO:
        System.out.println("LINETO " + "(" + coords[0] + ", " + coords[1] + ")");
        break;
      
      case PathIterator.SEG_QUADTO:
        System.out.println("QUADTO " + "(" + coords[0] + ", " + coords[1] + ")" + ", (" + coords[2]
            + ", " + coords[3] + ")");
        break;
      
      case PathIterator.SEG_CUBICTO:
        System.out.println("CUBICTO " + "(" + coords[0] + ", " + coords[1] + ")" + ", ("
            + coords[2] + ", " + coords[3] + ")" + ", (" + coords[4] + ", " + coords[5] + ")");
        break;
      
      default:
        System.out.println("Unknown type");
        break;
      }
    }
    
    System.out.println("fds fds fdasf sda fd");
    
  }
}
