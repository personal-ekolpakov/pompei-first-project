package kz.pompei.tanks;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import kz.pompei.tanks.world.World;

@SuppressWarnings("serial")
public class MainPanel extends JPanel {
  private final World world;
  
  public MainPanel(World world) {
    this.world = world;
  }
  
  @Override
  public void paint(Graphics g) {
    super.paint(g);
    paint2d((Graphics2D)g);
  }
  
  private void paint2d(Graphics2D g) {
    g.setColor(Color.white);
    g.fillRect(0, 0, getWidth(), getHeight());
    g.setColor(Color.black);
    g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
    
    world.paint(g, getWidth(), getHeight());
  }
}
