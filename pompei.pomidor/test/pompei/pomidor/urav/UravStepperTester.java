package pompei.pomidor.urav;

import org.junit.Assert;

public class UravStepperTester {
  private UravStepper urav;
  
  private float exactF(float t) {
    return (float)Math.pow((t * t + 2) / 3.0, 3.0 / 2.0);
  }
  
  public UravStepperTester(UravStepper urav) {
    this.urav = urav;
  }
  
  public void step1000() {
    float[] x = new float[] { 1 };
    urav.setK(0, x);
    for (int i = 1; i < urav.countOfK(); i++) {
      urav.setK(i, new float[1]);
    }
    urav.setFunction(new Function() {
      @Override
      public int size() {
        return 1;
      }
      
      @Override
      public void execute(float[] result, float t, float[] x) {
        result[0] = t * (float)Math.pow((double)x[0], 1.0 / 3.0);
      }
      
      @Override
      public float delta(float[] x1, float[] x2) {
        return Math.abs(x1[0] - x2[0]);
      }
    });
    
    float h = 0.001f;
    float t[] = new float[] { 1 };
    float maxDelta = 0;
    for (int i = 0; i < 1000; i++) {
      urav.step(t, h);
      float exact = exactF(t[0]);
      float delta = Math.abs(exact - x[0]);
      
      if (maxDelta < delta) maxDelta = delta;
      
      System.out.println("t = " + t[0] + ", exact = " + exact + ", calced = " + x[0] + ", dalta = "
          + delta);
    }
    
    System.out.println("maxDelta = " + maxDelta);
    Assert.assertTrue(maxDelta < h);
  }
}
