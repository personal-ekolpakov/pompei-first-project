package pompei.pomidor.urav.impl;

import org.junit.Test;

import pompei.pomidor.urav.UravStepper;
import pompei.pomidor.urav.UravStepperTester;

public class UravRungeKuttaTest {
  @Test
  public void steps1000() throws Exception {
    UravStepper urav = new UravRungeKutta();
    UravStepperTester tester = new UravStepperTester(urav);
    
    tester.step1000();
  }
}
