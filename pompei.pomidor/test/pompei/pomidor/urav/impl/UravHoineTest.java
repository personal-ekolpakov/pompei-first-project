package pompei.pomidor.urav.impl;

import org.junit.Test;

import pompei.pomidor.urav.UravStepper;
import pompei.pomidor.urav.UravStepperTester;

public class UravHoineTest {
  @Test
  public void steps1000() throws Exception {
    UravStepper urav = new UravHoine();
    UravStepperTester tester = new UravStepperTester(urav);
    
    tester.step1000();
  }
}
