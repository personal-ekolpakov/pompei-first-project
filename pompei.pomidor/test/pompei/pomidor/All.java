package pompei.pomidor;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import pompei.pomidor.urav.impl.UravHoineTest;
import pompei.pomidor.urav.impl.UravRungeKuttaTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({ UravHoineTest.class, UravRungeKuttaTest.class, })
public class All {}
