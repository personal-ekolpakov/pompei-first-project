package pompei.pomidor.urav;

public class FloatBuffer {
  private float[] buffer = null;
  private int size = 0;
  private float incSizeFactor = 1.5f;
  private boolean connected = false;
  
  public int next(int count) {
    if (count <= 0) throw new IllegalArgumentException("count = " + count
        + ", but must be more than zero");
    connected = false;
    int ret = size;
    size += count;
    return ret;
  }
  
  public float[] getBuffer() {
    if (connected) return buffer;
    if (buffer == null) {
      buffer = new float[size];
      connected = true;
      return buffer;
    }
    if (size <= buffer.length) {
      connected = true;
      return buffer;
    }
    
    {
      int newLength = (int)((float)buffer.length * incSizeFactor);
      if (newLength < size) newLength = size;
      float newBuffer[] = new float[newLength];
      System.arraycopy(buffer, 0, newBuffer, 0, newLength);
      buffer = newBuffer;
      connected = true;
      return newBuffer;
    }
  }
}
