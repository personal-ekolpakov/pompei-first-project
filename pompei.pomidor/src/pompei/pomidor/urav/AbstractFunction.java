package pompei.pomidor.urav;

public abstract class AbstractFunction implements Function {
  @Override
  public float delta(float[] x1, float[] x2) {
    float ret = 0;
    for (int i = 0, C = size(); i < C; i++) {
      float delta = Math.abs(x1[i] - x2[i]);
      if (ret < delta) ret = delta;
    }
    return ret;
  }
  
}
