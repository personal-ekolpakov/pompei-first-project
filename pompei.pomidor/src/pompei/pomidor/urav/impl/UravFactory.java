package pompei.pomidor.urav.impl;

import pompei.pomidor.urav.UravStepper;

public class UravFactory {
  public static UravStepper getHoineUrav() {
    return new UravHoine();
  }
  
  public static UravStepper getRungeKuttaUrav() {
    return new UravHoine();
  }
}
