package pompei.pomidor.urav.impl;

import pompei.pomidor.urav.Function;
import pompei.pomidor.urav.UravStepper;

/**
 * Формула Хойне - порядок точности - 4
 * 
 * @author pompei
 * 
 */
class UravHoine implements UravStepper {
  
  @Override
  public int getPrecisionExp() {
    return 4;
  }
  
  private Function f;
  
  private float k0[], k1[], k2[], k3[], k4[];
  
  @Override
  public int countOfK() {
    return 5;
  }
  
  @Override
  public void setK(int index, float[] k) {
    if (index == 0) {
      k0 = k;
      return;
    }
    if (index == 1) {
      k1 = k;
      return;
    }
    if (index == 2) {
      k2 = k;
      return;
    }
    if (index == 3) {
      k3 = k;
      return;
    }
    if (index == 4) {
      k4 = k;
      return;
    }
    
    throw new IllegalArgumentException("index must be one of 0,1,2,3,4");
  }
  
  @Override
  public void setFunction(Function f) {
    this.f = f;
  }
  
  @Override
  public void step(float[] tt, float h) {
    float t = tt[0];
    float h2 = h * 2;
    float h_2 = h / 2;
    
    int N = f.size();
    
    float[] k0 = this.k0;
    float[] k1 = this.k1;
    float[] k2 = this.k2;
    float[] k3 = this.k3;
    float[] k4 = this.k4;
    
    f.execute(k1, t, k0);
    
    for (int i = 0; i < N; i++) {
      k4[i] = k0[i] + h_2 * k1[i];
    }
    
    f.execute(k2, t + h_2, k4);
    
    for (int i = 0; i < N; i++) {
      k4[i] = k0[i] - h * k1[i] + h2 * k2[i];
    }
    
    f.execute(k3, t + h, k4);
    
    float h6 = h / 6;
    
    for (int i = 0; i < N; i++) {
      k0[i] += h6 * (k1[i] + 4 * k2[i] + k3[i]);
    }
    
    tt[0] += h;
  }
  
}
