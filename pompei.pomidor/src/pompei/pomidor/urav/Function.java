package pompei.pomidor.urav;

public interface Function {
  int size();
  
  void execute(float[] result, float t, float[] x);
  
  float delta(float[] x1, float[] x2);
}
