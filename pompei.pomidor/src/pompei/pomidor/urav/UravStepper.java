package pompei.pomidor.urav;

/**
 * Общий интерфейс расчёта дифура
 * 
 * @author pompei
 * 
 */
public interface UravStepper {
  /**
   * Получает порядок метода счёта
   * 
   * @return порядок метода счёта
   */
  int getPrecisionExp();
  
  /**
   * Устанавливает ссылку на соответствующий массив данных
   * 
   * @param index
   *          номер массива данных
   * @param k
   *          ссылка
   */
  void setK(int index, float[] k);
  
  /**
   * Количество используемых массивов (index в методе
   * {@link #setK(int, FloatBuffer)} должен быть меньше этого числа)
   * 
   * @return Количество используемых массивов
   */
  int countOfK();
  
  /**
   * Устанавливает функцию и размер массивов
   * 
   * @param f
   *          функция
   */
  void setFunction(Function f);
  
  /**
   * Производит один шаг
   * 
   * @param t
   *          массив состоящий из одного элемента - хранит время - после шага
   *          время увеличивается на h
   * @param h
   *          шаг операции по времени
   */
  void step(float t[], float h);
}
