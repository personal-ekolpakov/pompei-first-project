package pompei.pomidor.probes;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

public class PaintPicture {
  public static void main(String[] args) throws Exception {
    BufferedImage image = new BufferedImage(100, 50, BufferedImage.TYPE_INT_ARGB);
    
    Graphics2D g = image.createGraphics();
    
    {
      g.setColor(Color.WHITE);
      g.fillRect(0, 0, 100, 50);
      
      g.setColor(Color.BLACK);
      g.drawLine(10, 10, 70, 30);
      
      g.dispose();
    }
    
    File outFile = new File("out/saved.png");
    ImageIO.write(image, "png", outFile);
    
    System.out.println("OK");
  }
}
