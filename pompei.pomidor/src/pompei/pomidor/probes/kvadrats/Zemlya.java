package pompei.pomidor.probes.kvadrats;

import java.awt.Color;

public class Zemlya implements PointSource, HasPaint {
  private final Vec point;
  public final String name;
  
  public Zemlya(String name, Vec point) {
    this.name = name;
    Ma.notNull(point);
    this.point = point;
  }
  
  public Zemlya(String name, float x, float y) {
    this.name = name;
    this.point = new Vec(x, y);
  }
  
  @Override
  public Vec getPoint() {
    return point;
  }
  
  @Override
  public void paint(PaintContext c) {
    int x = c.X(point.x), y = c.Y(point.y);
    
    c.g.setColor(Color.green);
    
    c.g.drawLine(x - 7, y - 7, x + 7, y - 7);
    c.g.drawLine(x - 7, y - 9, x + 7, y - 9);
    
    c.g.drawLine(x - 5, y - 7, x, y);
    c.g.drawLine(x, y, x + 5, y - 7);
    
    c.g.fillOval(x - 3, y - 3, 7, 7);
  }
}
