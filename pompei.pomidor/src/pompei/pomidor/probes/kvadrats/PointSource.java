package pompei.pomidor.probes.kvadrats;

public interface PointSource {
  Vec getPoint();
}
