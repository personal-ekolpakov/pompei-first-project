package pompei.pomidor.probes.kvadrats;

public class Vec {
  public float x, y;
  
  public Vec() {}
  
  @Override
  public String toString() {
    return "Vec(" + x + ", " + y + ")";
  }
  
  public static final Vec ZERO = new Vec(0, 0);
  public static final Vec I = new Vec(1, 0);
  public static final Vec J = new Vec(0, 1);
  
  public Vec(float x, float y) {
    this.x = x;
    this.y = y;
  }
  
  public float sqr() {
    return x * x + y * y;
  }
  
  public float len() {
    return Ma.sqrt(sqr());
  }
  
  public Vec mul(float a) {
    return new Vec(a * x, a * y);
  }
  
  public Vec plus(Vec a) {
    return new Vec(a.x + x, a.y + y);
  }
  
  public Vec mnus(Vec a) {
    return new Vec(x - a.x, y - a.y);
  }
  
  /**
   * Повернуть вектор на угол fi
   * 
   * @param fi
   *          угол поворота
   * @return повёрнутый угол
   */
  public Vec turn(float fi) {
    return new Vec(x * Ma.cos(fi) - y * Ma.sin(fi), x * Ma.sin(fi) + y * Ma.cos(fi));
  }
  
  public float m(Vec a) {
    return x * a.x + y * a.y;
  }
}
