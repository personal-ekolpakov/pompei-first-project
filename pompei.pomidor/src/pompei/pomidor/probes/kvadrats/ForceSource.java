package pompei.pomidor.probes.kvadrats;

public interface ForceSource extends PointSource {
  Vec getForce();
}
