package pompei.pomidor.probes.kvadrats;

public class Ma {
  public static float sin(float a) {
    return (float)Math.sin(a);
  }
  
  public static float cos(float a) {
    return (float)Math.cos(a);
  }
  
  public static float sqrt(float a) {
    return (float)Math.sqrt(a);
  }
  
  public static void notNull(Object object) {
    if (object != null) return;
    throw new NullPointerException("It must be not null");
  }
}
