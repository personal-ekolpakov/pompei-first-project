package pompei.pomidor.probes.kvadrats;

public class Ka {
  
  public static void
  
  connect(Verevka V, Kvadrat K1, float a1, float b1, Kvadrat K2, float a2, float b2) {
    PointSource ps1 = K1.addConnect(new Vec(a1, b1), V.getAfs());
    PointSource ps2 = K2.addConnect(new Vec(a2, b2), V.getBfs());
    
    V.setA(ps1);
    V.setB(ps2);
  }
  
  public static void connect(Verevka V, Kvadrat K, float a, float b, Zemlya Z) {
    PointSource ps = K.addConnect(new Vec(a, b), V.getAfs());
    V.setA(ps);
    
    V.setB(Z);
  }
  
}
