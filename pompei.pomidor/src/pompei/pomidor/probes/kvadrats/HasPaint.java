package pompei.pomidor.probes.kvadrats;

public interface HasPaint {
  void paint(PaintContext c);
}
