package pompei.pomidor.probes.kvadrats;

import java.awt.Color;

public class Verevka implements HasPaint {
  private float K, L0;
  public final String name;
  
  public Verevka(String name, float K, float L0) {
    this.name = name;
    this.K = K;
    this.L0 = L0;
  }
  
  private PointSource A, B;
  
  public void setA(PointSource a) {
    A = a;
    updateFSs();
  }
  
  public void setB(PointSource b) {
    B = b;
    updateFSs();
  }
  
  private final FS Afs = new FS(), Bfs = new FS();
  
  private void updateFSs() {
    Afs.from = A;
    Afs.to = B;
    
    Bfs.from = B;
    Bfs.to = A;
  }
  
  public ForceSource getAfs() {
    return Afs;
  }
  
  public ForceSource getBfs() {
    return Bfs;
  }
  
  private class FS implements ForceSource {
    PointSource from, to;
    
    @Override
    public Vec getPoint() {
      return from.getPoint();
    }
    
    @Override
    public Vec getForce() {
      Vec L = to.getPoint().mnus(from.getPoint());
      
      float len = L.len();
      
      if (len <= L0) return Vec.ZERO;
      return L.mul(K / len);
    }
  }
  
  @Override
  public void paint(PaintContext c) {
    Vec p1 = A.getPoint();
    Vec p2 = B.getPoint();
    
    if (p1.mnus(p2).len() < L0) {
      c.g.setColor(Color.gray);
      c.line(p1, p2);
    } else {
      c.g.setColor(Color.black);
      c.line(p1, p2);
    }
  }
}
