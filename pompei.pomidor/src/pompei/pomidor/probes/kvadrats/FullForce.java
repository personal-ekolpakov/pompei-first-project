package pompei.pomidor.probes.kvadrats;

public class FullForce {
  public float Fx, Fy, M;
  
  public FullForce(float Fx, float Fy, float M) {
    this.Fx = Fx;
    this.Fy = Fy;
    this.M = M;
  }
  
  public FullForce(Vec F, float M) {
    this.Fx = F.x;
    this.Fy = F.y;
    this.M = M;
  }
  
  public FullForce() {}
  
  public Vec getF() {
    return new Vec(Fx, Fy);
  }
  
  public void setF(Vec F) {
    Fx = F.x;
    Fy = F.y;
  }
}
