package pompei.pomidor.probes.kvadrats;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import pompei.pomidor.urav.AbstractFunction;
import pompei.pomidor.urav.UravStepper;
import pompei.pomidor.urav.impl.UravFactory;

public class LauncherKvadrats {
  public static void main(String[] args) throws Exception {
    new LauncherKvadrats().run();
    System.out.println("COMPLETE");
  }
  
  final float g = 9.8f;
  
  private void run() throws Exception {
    Zemlya Z1 = new Zemlya("Z1", 7.5f, 13f);
    Zemlya Z2 = new Zemlya("Z2", 11.5f, 11f);
    
    Kvadrat K1 = new Kvadrat("K1", 1.5f, 1.25f, 0.75f);
    K1.setPosition(5.25f, 10.75f, 0);
    
    Kvadrat K2 = new Kvadrat("K2", 1.0f, 1.25f, 0.5f);
    K2.setPosition(2.25f, 8.5f, 0);
    
    Kvadrat K3 = new Kvadrat("K3", 0.9f, 0.75f, 0.75f);
    K3.setPosition(5.25f, 5.75f, 0);
    
    Kvadrat K4 = new Kvadrat("K4", 1.4f, 1.75f, 0.5f);
    K4.setPosition(8.75f, 8f, 0);
    
    Verevka V1 = new Verevka("V1", 500f, 4f);
    Verevka V2 = new Verevka("V2", 100f, 4f);
    Verevka V3 = new Verevka("V3", 1f, 4f);
    Verevka V4 = new Verevka("V4", 500f, 4f);
    Verevka V5 = new Verevka("V5", 50f, 4f);
    
    Ka.connect(V2, K2, +1.25f, +0.50f, K1, -0.75f, -0.75f);
    Ka.connect(V3, K1, +0.75f, -0.75f, K4, -1.75f, +0.50f);
    Ka.connect(V5, K2, +0.00f, -0.50f, K3, -0.00f, +0.75f);
    
    Ka.connect(V1, K1, +1.25f, +0.75f, Z1);
    Ka.connect(V4, K4, +0.75f, +0.50f, Z2);
    
    List<HasPaint> paintList = new ArrayList<>();
    paintList.add(V1);
    paintList.add(V2);
    paintList.add(V3);
    paintList.add(V4);
    paintList.add(V5);
    paintList.add(K1);
    paintList.add(K2);
    paintList.add(K3);
    paintList.add(K4);
    paintList.add(Z1);
    paintList.add(Z2);
    
    Fun fun = new Fun();
    fun.kvadratList.add(K1);
    fun.kvadratList.add(K2);
    fun.kvadratList.add(K3);
    fun.kvadratList.add(K4);
    
    UravStepper urav = UravFactory.getHoineUrav();
    urav.setFunction(fun);
    
    float x[] = new float[fun.size()];
    urav.setK(0, x);
    for (int i = 1, C = urav.countOfK(); i < C; i++) {
      urav.setK(i, new float[fun.size()]);
    }
    
    fun.distributeAndReset(x);
    
    float h = 0.01f;
    float t[] = new float[] { 0 };
    print(paintList, fun.kvadratList, t[0], 0);
    
    if ("a".equals("a1")) return;
    
    for (int i = 1; i <= 3000; i++) {
      urav.step(t, h);
      
      print(paintList, fun.kvadratList, t[0], i);
    }
    
  }
  
  float lastPrintTime = -1000;
  int nextFrameNumber = 1;
  
  private void print(List<HasPaint> paintList, List<Kvadrat> kvadratList, float t, int i)
      throws Exception {
    if (t - lastPrintTime >= 1f / 24f) {
      int frameNo = nextFrameNumber++;
      paintPicture(paintList, frameNo, t);
      lastPrintTime = t;
      
      System.out.println("i-FRAME = " + i + "-" + frameNo + ", t = " + t);
    }
  }
  
  private class Fun extends AbstractFunction {
    final List<Kvadrat> kvadratList = new ArrayList<>();
    
    class MyOuterDo implements OuterDo {
      @Override
      public FullForce getOuterDo(Kvadrat K) {
        FullForce ret = new FullForce();
        ret.M = 0;
        ret.Fx = 0;
        ret.Fy = -g * K.m;
        
        float Ktr = 0.5f;
        
        Vec Ftr = K.getV().mul(-Ktr);
        float Mtr = -K.getW() * Ktr;
        
        ret.M += Mtr;
        ret.Fx += Ftr.x;
        ret.Fy += Ftr.y;
        
        return ret;
      }
    }
    
    final OuterDo outerDo = new MyOuterDo();
    
    @Override
    public int size() {
      return kvadratList.size() * Kvadrat.getBufferSize();
    }
    
    public void distributeAndReset(float[] x) {
      int offset = 0;
      for (Kvadrat K : kvadratList) {
        offset = K.setBufferOffset(offset);
        K.setBuffer(x, true);
      }
    }
    
    @Override
    public void execute(float[] result, float t, float[] x) {
      for (Kvadrat K : kvadratList) {
        K.setBuffer(x, false);
        K.executeUrav(t, outerDo, result);
      }
    }
  }
  
  private void paintPicture(List<HasPaint> paintList, int frameNo, float t) throws Exception {
    int width = 18 * 40, height = 14 * 40;
    
    PaintContext c = new PaintContext(width, height);
    
    BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    c.g = image.createGraphics();
    c.sx = 40;
    c.sy = 40;
    
    c.g.setColor(Color.white);
    c.g.fillRect(0, 0, width, height);
    
    for (HasPaint hasPaint : paintList) {
      hasPaint.paint(c);
    }
    
    c.g.setColor(Color.black);
    c.g.drawString("FRAME " + frameNo + ", t = " + t + "s", 10, 10);
    
    c.g.dispose();
    
    {
      String s = "" + frameNo;
      while (s.length() < 4)
        s = "0" + s;
      File outFile = new File("out/Kvadrats/pictures/Kvadrats" + s + ".png");
      ImageIO.write(image, "png", outFile);
    }
  }
}
