package pompei.pomidor.probes.kvadrats;

import java.awt.Graphics2D;

public class PaintContext {
  public Graphics2D g;
  
  public final int width, height;
  public float sx, sy;
  
  public int X(float x) {
    return (int)(x * sx + 0.5f);
  }
  
  public int Y(float y) {
    return height - (int)(y * sy + 0.5f);
  }
  
  public PaintContext(int width, int height) {
    this.width = width;
    this.height = height;
  }
  
  public void line(Vec a, Vec b) {
    g.drawLine(X(a.x), Y(a.y), X(b.x), Y(b.y));
  }
  
  public void point(Vec p) {
    int x = X(p.x), y = Y(p.y);
    g.fillOval(x - 3, y - 3, 7, 7);
  }
}
