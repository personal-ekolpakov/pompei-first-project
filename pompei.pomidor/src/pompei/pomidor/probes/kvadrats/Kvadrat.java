package pompei.pomidor.probes.kvadrats;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Kvadrat implements HasPaint {
  private float[] buffer;
  private int bufferOffset;
  
  public float x0 = 0, y0 = 0, fi0 = 0, vx0 = 0, vy0 = 0, w0 = 0;
  public float m, a, b;
  public final String name;
  
  @Override
  public String toString() {
    return "Kvadrat " + name + ", C = " + getCenter() + ", fi = " + getFI() + ", v = " + getV()
        + ", w = " + getW();
  }
  
  public Kvadrat(String name, float m, float a, float b) {
    this.name = name;
    this.m = m;
    this.a = a;
    this.b = b;
  }
  
  public float getJ() {
    return (a * a + b * b) * m / 3f;
  }
  
  public int setBufferOffset(int bufferOffset) {
    this.bufferOffset = bufferOffset;
    return bufferOffset + getBufferSize();
  }
  
  public void setBuffer(float[] buffer, boolean reset) {
    this.buffer = buffer;
    
    if (reset) {
      buffer[bufferOffset + 0] = x0;
      buffer[bufferOffset + 1] = y0;
      buffer[bufferOffset + 2] = fi0;
      buffer[bufferOffset + 3] = vx0;
      buffer[bufferOffset + 4] = vy0;
      buffer[bufferOffset + 5] = w0;
    }
  }
  
  public static int getBufferSize() {
    return 6;
  }
  
  public Vec getCenter() {
    if (buffer == null) return new Vec(x0, y0);
    return new Vec(buffer[bufferOffset + 0], buffer[bufferOffset + 1]);
  }
  
  public float getFI() {
    if (buffer == null) return fi0;
    return buffer[bufferOffset + 2];
  }
  
  public Vec getV() {
    if (buffer == null) return new Vec(vx0, vy0);
    return new Vec(buffer[bufferOffset + 3], buffer[bufferOffset + 4]);
  }
  
  public float getW() {
    if (buffer == null) return w0;
    return buffer[bufferOffset + 5];
  }
  
  private final List<ForceSource> fourceSourceList = new ArrayList<>();
  
  public PointSource addConnect(final Vec rel, ForceSource fs) {
    Ma.notNull(rel);
    Ma.notNull(fs);
    fourceSourceList.add(fs);
    
    return new PointSource() {
      @Override
      public Vec getPoint() {
        return getCenter().plus(rel.turn(getFI()));
      }
    };
  }
  
  public void setPosition(float x0, float y0, float fi0) {
    this.x0 = x0;
    this.y0 = y0;
    this.fi0 = fi0;
  }
  
  @Override
  public void paint(PaintContext c) {
    float fi = getFI();
    
    Vec ai = Vec.I.mul(a).turn(fi);
    Vec bi = Vec.J.mul(b).turn(fi);
    Vec C = getCenter();
    Vec U1 = C.plus(ai).plus(bi);
    Vec U2 = C.mnus(ai).plus(bi);
    Vec U3 = C.mnus(ai).mnus(bi);
    Vec U4 = C.plus(ai).mnus(bi);
    
    c.g.setColor(Color.black);
    c.line(U1, U2);
    c.line(U2, U3);
    c.line(U3, U4);
    c.line(U4, U1);
    
    c.g.setColor(Color.blue);
    for (ForceSource fs : fourceSourceList) {
      c.point(fs.getPoint());
    }
  }
  
  public void executeUrav(float t, OuterDo outerDo, float[] result) {
    FullForce f = outerDo.getOuterDo(this);
    
    for (ForceSource fs : fourceSourceList) {
      Vec Fu = fs.getForce();
      f.setF(f.getF().plus(Fu));
      
      Vec R = fs.getPoint().mnus(getCenter());
      f.M += -Fu.x * R.y + Fu.y * R.x;
    }
    
    result[bufferOffset + 0] = buffer[bufferOffset + 3];
    result[bufferOffset + 1] = buffer[bufferOffset + 4];
    result[bufferOffset + 2] = buffer[bufferOffset + 5];
    result[bufferOffset + 3] = f.Fx / m;
    result[bufferOffset + 4] = f.Fy / m;
    result[bufferOffset + 5] = f.M / getJ();
  }
}
