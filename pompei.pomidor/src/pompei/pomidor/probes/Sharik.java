package pompei.pomidor.probes;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import pompei.pomidor.urav.Function;
import pompei.pomidor.urav.UravStepper;
import pompei.pomidor.urav.impl.UravFactory;

public class Sharik {
  public static void main(String[] args) throws Exception {
    new Sharik().run();
  }
  
  float m = 1;
  float g = 10;
  float K = 450;
  float q = 0.4f;
  float H = 10;
  
  private float F(float x, float v) {
    return -m * g + Fupr(x, v);
  }
  
  private float Fupr(float x, float v) {
    if (x < 0) {
      if (v < 0) return -K * x;
      return -K * q * x;
    }
    return 0;
  }
  
  private class Fun implements Function {
    @Override
    public int size() {
      return 2;
    }
    
    @Override
    public float delta(float[] x1, float[] x2) {
      float ret = 0;
      for (int i = 0, C = size(); i < C; i++) {
        float delta = Math.abs(x1[i] - x2[i]);
        if (ret < delta) ret = delta;
      }
      return ret;
    }
    
    @Override
    public void execute(float[] result, float t, float[] x) {
      result[0] = x[1];
      result[1] = F(x[0], x[1]) / m;
    }
  }
  
  float lastPrintTime = -1000;
  int nextFrameNumber = 1;
  
  private void run() throws Exception {
    UravStepper urav = UravFactory.getHoineUrav();
    
    Fun fun = new Fun();
    urav.setFunction(fun);
    float x[] = new float[fun.size()];
    urav.setK(0, x);
    for (int i = 1, C = urav.countOfK(); i < C; i++) {
      urav.setK(i, new float[fun.size()]);
    }
    
    x[0] = H;
    x[1] = 0;
    float h = 0.01f;
    float t[] = new float[] { 0 };
    print(t, x, H, 0);
    for (int i = 1; i <= 5500; i++) {
      urav.step(t, h);
      print(t, x, H, i);
    }
    
    System.out.println("COMPLETE");
  }
  
  private void print(float[] t, float[] x, float H, int i) throws Exception {
    if (t[0] - lastPrintTime >= 1f / 24f) {
      paintPicture(nextFrameNumber++, t[0], x[0], H, i);
      lastPrintTime = t[0];
      
      System.out.println("i = " + i + ", t = " + t[0] + ", x = " + x[0] + ", v = " + x[1]
          + ", F = " + F(x[0], x[1]));
    }
    
  }
  
  private void paintPicture(int frameNo, float t, float x, float H, int i) throws Exception {
    int width = 300, height = 600;
    BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    
    Graphics2D g = image.createGraphics();
    
    float r = 30;
    float top = 30;
    float floor = 100;
    
    float D = (float)height - (top + r) - (floor + r);
    
    float L = D / H * x;
    
    float Y = height - floor - r - L;
    
    int x1 = (int)(width / 2 - r + 0.5f);
    int W = (int)(2f * r + 0.5f);
    
    {
      g.setColor(Color.WHITE);
      g.fillRect(0, 0, width, height);
      
      g.setColor(Color.BLACK);
      g.drawOval(x1, (int)(Y - r + 0.5f), W, W);
      
      {
        g.setColor(Color.BLUE);
        int TOP = (int)(height - floor + 0.5f);
        g.drawLine(0, TOP, width, TOP);
      }
      
      g.setColor(Color.BLUE);
      {
        String s = "FRAME " + frameNo + ", t = " + t + " c";
        
        g.drawString(s, 10, 10);
      }
      {
        String s = "i = " + i + ", x = " + x;
        
        g.drawString(s, 10, 20);
      }
      
      g.dispose();
    }
    
    {
      String s = "" + frameNo;
      while (s.length() < 4)
        s = "0" + s;
      File outFile = new File("out/Sharik/pictures/Sharik" + s + ".png");
      ImageIO.write(image, "png", outFile);
    }
  }
}
