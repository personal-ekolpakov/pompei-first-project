package pompei.pomidor.probes;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import pompei.pomidor.urav.AbstractFunction;
import pompei.pomidor.urav.Doiter;
import pompei.pomidor.urav.UravStepper;
import pompei.pomidor.urav.impl.UravFactory;

public class Kvadrat {
  public static void main(String[] args) throws Exception {
    new Kvadrat().run();
    System.out.println("Complete");
  }
  
  private static float sin(float x) {
    return (float)Math.sin(x);
  }
  
  private static float cos(float x) {
    return (float)Math.cos(x);
  }
  
  private static float sqrt(float x) {
    return (float)Math.sqrt(x);
  }
  
  private final float g = 9.8f;
  
  private final float Tx = 7.5f;
  private final float Ty = 16f;
  
  private final float a = 2.7f;
  private final float b = 1.8f;
  private final float m = 3;
  private final float J = (a * a + b * b) * m / 3f;
  
  private final float L0 = 3.5f;
  private final float Ku = 10f;
  
  private final float kTRv = 0.3f;
  private final float kTRw = 0.3f;
  
  private float __Fx, __Fy, __M;
  
  private class Fun extends AbstractFunction {
    @Override
    public int size() {
      return 6;
    }
    
    @Override
    public void execute(float[] r, float t, float[] massive) {
      float x = massive[0];
      float y = massive[1];
      float fi = massive[2];
      float vx = massive[3];
      float vy = massive[4];
      float w = massive[5];
      
      r[0] = vx;
      r[1] = vy;
      r[2] = w;
      
      float Ux = x + a * cos(fi) - b * sin(fi);
      float Uy = y + a * sin(fi) + b * cos(fi);
      
      float UTx = Tx - Ux;
      float UTy = Ty - Uy;
      float UT = sqrt(UTx * UTx + UTy * UTy);
      
      float Fu = UT <= L0 ? 0 :(UT - L0) * Ku;
      float Fux = UTx * Fu / UT;
      float Fuy = UTy * Fu / UT;
      
      float FTRx = -m * kTRv * vx;
      float FTRy = -m * kTRv * vy;
      
      float Fx = Fux + FTRx;
      float Fy = Fuy + FTRy - m * g;
      
      __Fx = Fx;
      __Fy = Fy;
      
      float AUx = Ux - x;
      float AUy = Uy - y;
      
      float Mu = -Fux * AUy + Fuy * AUx;
      
      float MTR = -J * kTRw * w;
      
      float M = Mu + MTR;
      __M = M;
      
      r[3] = Fx / m;
      r[4] = Fy / m;
      r[5] = M / J;
    }
  }
  
  private static final float PI = (float)Math.PI;
  private static final float PI2 = 2f * PI;
  
  private void run() throws Exception {
    UravStepper urav = UravFactory.getHoineUrav();
    
    Fun fun = new Fun();
    urav.setFunction(fun);
    
    float x[] = new float[fun.size()];
    urav.setK(0, x);
    for (int i = 1, C = urav.countOfK(); i < C; i++) {
      urav.setK(i, new float[fun.size()]);
    }
    
    x[0] = 4.25f;
    x[1] = 5.5f + 8f;
    x[2] = 0;
    x[3] = 0;
    x[4] = 0;
    x[5] = 0;
    
    float h = 0.01f;
    float t[] = new float[] { 0 };
    print(t, x, 0);
    for (int i = 1; i <= 3000; i++) {
      urav.step(t, h);
      
      if (i % 10 == 0) {
        float fi = x[2];
        while (fi > PI) {
          fi -= PI2;
        }
        while (fi < -PI) {
          fi += PI2;
        }
        x[2] = fi;
      }
      
      print(t, x, i);
    }
  }
  
  float lastPrintTime = -1000;
  int nextFrameNumber = 1;
  
  private void print(float[] t, float[] x, int i) throws Exception {
    if (t[0] - lastPrintTime >= 1f / 24f) {
      int frameNo = nextFrameNumber++;
      paintPicture(x[0], x[1], x[2], frameNo, t[0]);
      lastPrintTime = t[0];
      
      System.out.println("i-FRAME = " + i + "-" + frameNo + ", t = " + t[0] + ", x = " + x[0]
          + ", y = " + x[1] + ", fi = " + (x[2] * 180f / 3.1415926f) + ", w = " + x[5] + ", Fx = "
          + __Fx + ", Fy = " + __Fy + ", M = " + __M);
    }
  }
  
  private void paintPicture(final float x, final float y, final float fi, final int frameNo,
      final float t) throws Exception {
    new Doiter() {
      int width = 480;
      int height = 800;
      float sx = 40, sy = 40;
      private Graphics2D g;
      
      int X(float x) {
        return (int)(x * sx + 0.5f);
      }
      
      int Y(float y) {
        return height - (int)(y * sy + 0.5f);
      }
      
      @Override
      public void doit() throws Exception {
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        g = image.createGraphics();
        
        g.setColor(Color.white);
        g.fillRect(0, 0, width, height);
        
        g.setColor(Color.black);
        
        float U1x = x + a * cos(fi) - b * sin(fi);
        float U1y = y + a * sin(fi) + b * cos(fi);
        
        float U2x = x - a * cos(fi) - b * sin(fi);
        float U2y = y - a * sin(fi) + b * cos(fi);
        
        float U3x = x - a * cos(fi) + b * sin(fi);
        float U3y = y - a * sin(fi) - b * cos(fi);
        
        float U4x = x + a * cos(fi) + b * sin(fi);
        float U4y = y + a * sin(fi) - b * cos(fi);
        
        line(U1x, U1y, U2x, U2y);
        line(U2x, U2y, U3x, U3y);
        line(U3x, U3y, U4x, U4y);
        line(U4x, U4y, U1x, U1y);
        
        float UTx = U1x - Tx;
        float UTy = U1y - Ty;
        float UT = sqrt(UTx * UTx + UTy * UTy);
        
        if (UT < L0) {
          //g.setColor(Color.black);
          //line(U1x, U1y, Tx, Ty);
        } else {
          g.setColor(Color.black);
          line(U1x, U1y, Tx, Ty);
        }
        
        g.setColor(Color.blue);
        point(U1x, U1y);
        
        g.setColor(Color.GREEN);
        ground(Tx, Ty);
        
        g.setColor(Color.black);
        g.drawString("FRAME " + frameNo + ", t = " + t, 10, 10);
        
        g.dispose();
        
        {
          String s = "" + frameNo;
          while (s.length() < 4)
            s = "0" + s;
          File outFile = new File("out/Kvadrat/pictures/Kvadrat" + s + ".png");
          ImageIO.write(image, "png", outFile);
        }
      }
      
      private void ground(float xf, float yf) {
        int x = X(xf), y = Y(yf);
        g.drawLine(x - 7, y - 7, x + 7, y - 7);
        g.drawLine(x - 7, y - 9, x + 7, y - 9);
        
        g.drawLine(x - 5, y - 7, x, y);
        g.drawLine(x, y, x + 5, y - 7);
        
        g.fillOval(x - 3, y - 3, 6, 6);
      }
      
      private void point(float x, float y) {
        g.fillOval(X(x) - 4, Y(y) - 4, 8, 8);
      }
      
      private void line(float x1, float y1, float x2, float y2) {
        g.drawLine(X(x1), Y(y1), X(x2), Y(y2));
      }
    }.doit();
    
  }
}
