package kz.pompei.gui.awt;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class AsdFrame extends JFrame {
  public static void main(String[] args) {
    AsdFrame f = new AsdFrame();
    f.setVisible(true);
  }
  
  public AsdFrame() {
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setTitle("Hello world!!!");
    setBounds(100, 50, 800, 600);
    
    setContentPane(new AsdPanel());
  }
}
