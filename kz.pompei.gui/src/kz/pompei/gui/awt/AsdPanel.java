package kz.pompei.gui.awt;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class AsdPanel extends JPanel {
  
  private class MouseCursor {
    
    private boolean visible = false;
    int currentX = 0, currentY = 0;
    
    void move(Graphics g, int x, int y) {
      drawMe(g);
      currentX = x;
      currentY = y;
      drawMe(g);
    }
    
    void setVisible(Graphics g, boolean visible) {
      boolean oldVisible = this.visible;
      this.visible = visible;
      
      if (oldVisible != visible) {
        drawMe(g);
      }
    }
    
    private void drawMe(Graphics g) {
      g.setXORMode(Color.white);
      g.drawLine(currentX - 5, currentY, currentX + 5, currentY);
      g.drawLine(currentX, currentY - 5, currentX, currentY + 5);
    }
  }
  
  private final MouseCursor mouseCursor = new MouseCursor();
  
  @Override
  public void paint(Graphics g) {
    super.paint(g);
    
    for (AwtButton b : buttons) {
      b.paint(g);
    }
  }
  
  private final List<AwtButton> buttons = new ArrayList<>();
  
  public AsdPanel() {
    {
      BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
      
      Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg,
          new Point(0, 0), "blank cursor");
      
      setCursor(blankCursor);
    }
    
    buttons.add(new AwtButton(10, 10, 100, 30));
    buttons.add(new AwtButton(10, 60, 100, 30));
    
    addMouseHandlers();
  }
  
  private void addMouseHandlers() {
    addMouseListener(new MouseListener() {
      
      @Override
      public void mouseReleased(MouseEvent e) {
        // TODO implement
        
      }
      
      @Override
      public void mousePressed(MouseEvent e) {}
      
      @Override
      public void mouseExited(MouseEvent e) {
        Graphics g = getGraphics();
        mouseCursor.setVisible(g, false);
        g.dispose();
      }
      
      @Override
      public void mouseEntered(MouseEvent e) {
        Graphics g = getGraphics();
        mouseCursor.setVisible(g, true);
        mouseCursor.move(g, e.getX(), e.getY());
        g.dispose();
      }
      
      @Override
      public void mouseClicked(MouseEvent e) {
        // TODO implement
        
      }
    });
    
    addMouseMotionListener(new MouseMotionListener() {
      
      @Override
      public void mouseMoved(MouseEvent e) {
        Graphics g = getGraphics();
        mouseCursor.move(g, e.getX(), e.getY());
        g.dispose();
      }
      
      @Override
      public void mouseDragged(MouseEvent e) {
        Graphics g = getGraphics();
        mouseCursor.move(g, e.getX(), e.getY());
        g.dispose();
      }
    });
  }
  
}
