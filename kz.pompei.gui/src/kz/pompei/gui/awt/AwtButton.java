package kz.pompei.gui.awt;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class AwtButton {
  private final Rectangle rectangle = new Rectangle();
  @SuppressWarnings("unused")
  private boolean mouseOver;
  
  public AwtButton(int x, int y, int width, int height) {
    rectangle.x = x;
    rectangle.y = y;
    rectangle.width = width;
    rectangle.height = height;
  }
  
  public boolean contains(int x, int y) {
    return rectangle.contains(x, y);
  }
  
  private Color borderColor = Color.black;
  
  public void setMouseOver(boolean mouseOver) {
    this.mouseOver = mouseOver;
    
    borderColor = mouseOver ? Color.blue :Color.green;
  }
  
  public void paint(Graphics graphics) {
    Graphics g = graphics.create();
    g.setColor(borderColor);
    g.drawRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
    g.dispose();
  }
}
